f = open('log.txt', 'r')

classifications = {'ERROR': [],
                    'WARN': [],
                    'DEBUG': [],
                    'OTHER': []}
                    
def get_pref(line):
    pref_start = line.index('[')
    pref_end = line.index(']')
    pref = line[pref_start+1 : pref_end]
    return pref, pref_end

for line in f:
    pref, pref_end  = get_pref(line)
    
    if pref not in classifications:
        pref = 'OTHER'
    
    classifications[pref].append(
                            line[pref_end+2 : -1])

print(classifications)