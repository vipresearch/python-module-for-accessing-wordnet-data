from wordnet import WordNet

classifications = {'informational': [], 'cautionary': [], 'fatal': []}

def get_pref(line):
    pref_start = line.index('[')
    pref_end = line.index(']')
    pref = line[pref_start+1 : pref_end]
    return pref, pref_end

with open('log.txt', 'r') as f, WordNet() as wn:
    for line in f:
        pref, pref_end  = get_pref(line)
    
        best_cost, best_cls = 2**31-1, ''
        for cls in classifications:
            cost, _ = wn.path_between_words(pref, cls)
            if cost < best_cost:
                best_cost = cost
                best_cls = cls
        classifications[best_cls].append(
                                    line[pref_end+2 : -1])

print(classifications)
