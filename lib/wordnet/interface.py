"""
Module that defines an interface to WordNet.
TODO: Remove debug print messages.
"""

#Imports used by all WordNet classes
from wordnet import wn_db_location

#Imports used by WordNetBase
from os import O_RDONLY, open as open2fd, close as closefd #database
from mmap import mmap, ACCESS_READ, ACCESS_WRITE

#Imports used by WordNetManager
from multiprocessing.connection import wait #subprocess messaging
from threading import Thread #subprocess messaging
from multiprocessing import cpu_count #subprocess creation
from multiprocessing import Pipe #subprocess messaging
from multiprocessing import current_process #exception propagation
from multiprocessing import active_children #reaping subprocesses
from collections import deque #task queue
from signal import signal, SIGTERM #exception propagation
from os import kill #used in signal handler
from sys import stdout #flushes to generate HTML ProgressEvents

from .utils import handle_exception_packet #exception propagation

#Imports used by WordNetResult
from multiprocessing import TimeoutError #task completion
from threading import Event #task completion

#Imports used by WordNetCapabilities
import heapq #path_between_words()
from .utils import binary_search_index #database
from .utils import default_cost_func #path_between_words()

#Imports used by WordNetWorker
from multiprocessing import Process #inheritance, also metaclass
from .utils import build_exception_packet #exception propagation

# https://wordnet.princeton.edu/documentation/wndb5wn
# https://wordnet.princeton.edu/documentation/wninput5wn

class SynSet:
    """
    Class which encapsulates the data of one Synset in the WordNet
    database.

    Instance Attributes:
        offset: An 8-digit decimal string representing the byte offset
                from the start of the data file for the part of speech
                containing this synset.
        type: A single character representing the part of speech to
              which this synset applies.  See also: WordNet.pos
        words: A set containing the words in this synset.  At this
               time, the lexicographical id for each word is discarded.
        pointers: A tuple of dictionaries, where each dictionary holds
                  one pointer.  Each pointer has four parts as follows:
                  symbol: A pointer symbol denoting the relationship
                          between the source synset/word and the target
                          synset/word.  Consult the WordNet
                          documentation for further explanation.
                  offset: The byte offset of the synset being pointed
                          to, as described in the offset attribute of
                          the Synset class
                  pos: The part of speech to which the synset belongs
                  relation: A 4-digit hexadecimal number representing
                            the relationship between the source and
                            target synsets.  The first two digits
                            identify the index of the word in the source
                            synset; the last two identify the word in
                            the target synset.  Both are 1-indexed.
                            If the field consists of 4 zeros, then the
                            relation is semantic and involves both
                            synsets in their entirety; otherwise the
                            relation is lexical between the two
                            identified words.  Since word order is
                            not preserved at this time, only 0000
                            relations are meaningful.
    """
    def __init__(self, splitline):
        self.offset = splitline[0]
        #Lexicographer file number, unnecessary right now

        self.type = splitline[2] # 'n' 'v' 'a' 's' 'r' POS, really

        self.prev = None

        wd_cnt = int(splitline[3], 16)
        # Right now, don't need the lexicographer file id for each word
        # Nor do we need the order of the words for lexical relations
        self.words = {splitline[i].lower(): splitline[i] for i in range(4, (2*wd_cnt)+4, 2)}

        ptr_offset = 2*wd_cnt + 5
        self.pointers = [{'symbol': splitline[i], 'offset': splitline[i+1],
            'pos': splitline[i+2], 'relation': splitline[i+3]} for i in
            range(ptr_offset, 4*int(splitline[ptr_offset-1]) + ptr_offset, 4)]

    def follows(self, synset):
        """
        Set the previous SynSet in the path to the given SynSet,
        and return the current SynSet.
        """
        self.prev = synset
        return self

    def __lt__(self, other):
        """
        Dummy implementation of less-than operator.
        Only defined so it can be 'ignored' by heapq module during path search.
        """
        return False

class WordNetResult:
    """
    Class representing the result of a WordNet operation.
    Most WordNet operation calls return immediately with an instance
    of this class as the result, and the main program can continue as
    normal.  When the actual result of the operation is needed, it can
    be retrieved from the result object.  The main program will block
    until the result is computed, or the result will be returned without
    any blocking if it is ready.

    Instance Attributes:
        req: The method name and parameters for the particular operation
    Private Attributes:
        _evt: A threading.Event object to wait on for the completed
              result, should the operation be incomplete.
        _data: Exists only on completed WordNetResult instances.  Should
               only be accessed by calls to WordnetResult.get()
    """
    def __init__(self, req):
        self.req = req
        self._evt = Event()
    def get(self, timeout=None):
        """
        Waits for this operation result to complete, or as long as the
        specified timeout if the operation has not finished.  Returns
        immediately if the result is ready.  Raises a TimeoutError if
        a timeout is given and the result is incomplete by that time.

        Parameters:
            self: The WordNetResult instance to wait for.
            timeout: Optionally specify the maximum amount of time to
                     block execution.  Otherwise block until the
                     computation finishes.
        Returns:
            The return value of the operation called.  See documentation
            of specific method calls to see detailed return information.
        """
        if not hasattr(self,'_data'):
            print('waiting on',self.req,end='</br>')
        if self._evt.wait(timeout):
            return self._data
        raise TimeoutError('Result for %s timed out'%self.req)


class WordNetMeta(type):
    """
    Class to handle the creation of the WordNet interface.

    TODO:
    Make this actually do something useful, so that the WordNet class and
    it's workers can be cleaner and more pragmatic
    Such as: make multiprocessing optional. WordNet will function as it
    currently does if a keyword argument is given, otherwise it will
    run sequentially with regular code, to simplify programming for
    non-performance-critical tasks
    """
    registry = {}
    def __new__(metacls, newcls, bases, attrs, **kwargs):
        #Returns a class
        newcls = super().__new__(metacls, newcls, bases, attrs)
        if 'purpose' in kwargs:
            metacls.registry[kwargs['purpose']] = newcls
        return newcls
    def __init__(self, *args, **kargs):
        #Required by python 3.5 or lower; type doesn't ignore
        #extra keyword arguments out of the box
        return super().__init__(*args)

def WordNet(db_root=wn_db_location, multiprocessing=False):
    capabilities = WordNetMeta.registry['capability_provider'](db_root)
    if multiprocessing:
        return WordNetMeta.registry['manager'](capabilities, WordNetMeta.registry['worker'], db_root)
    else:
        return capabilities

class WordNetBase(metaclass=WordNetMeta):
    message_types = {'COMPLETED_RESULT':0, 'EXCEPTION':1, 'SYNSET_REQUEST':2}
    _sentinel = object()

    def __init__(self, db_root=wn_db_location):
        self.dbroot = db_root

        self.pos = {'n': 'noun', 'v': 'verb', 'r': 'adv', 'a': 'adj'}

        self.dbfiles = {'index': {}, 'data': {}, 'exc': {}}

        self.synsets = {}

        self.requestsMade = 0
        self.requestOffset = 0
        self.requestsComplete = 0
        self.requests = deque()
        self.pending = {}
        self.available = deque()

    def __enter__(self):
        """
        Initialize database for use.
        If a file fails to open, the exception is not caught.
        See also: Python Context Manager Protocol
        """
        print('starting',self)
        self.memory = mmap(-1, 30*1024*1024, access=ACCESS_READ|ACCESS_WRITE)

        # https://en.wikipedia.org/wiki/Quadratic_probing
        # https://en.wikipedia.org/wiki/Hopscotch_hashing
        self.synset_index = mmap(-1, 30*1024*1024, access=ACCESS_READ|ACCESS_WRITE)

        for typ in self.dbfiles:
            if typ == 'exc':
                file_offsets = {}
                for ps in self.pos:
                    tmp = open2fd(self.dbroot + self.pos[ps] + '.exc', O_RDONLY)
                    tmpmap = mmap(tmp, 0, access=ACCESS_READ)
                    self.memory.write(tmpmap[:])
                    file_offsets[ps] = (self.memory.tell() - tmpmap.size(),
                            self.memory.tell())
                    tmpmap.close()
                    closefd(tmp)
            else:
                file_offsets = {}
                for ps in self.pos:
                    tmp = open2fd(self.dbroot + typ + '.' + self.pos[ps], O_RDONLY)
                    tmpmap = mmap(tmp, 0, access=ACCESS_READ)
                    self.memory.write(tmpmap[:])
                    file_offsets[ps] = (self.memory.tell() - tmpmap.size(),
                            self.memory.tell())
                    tmpmap.close()
                    closefd(tmp)
            self.dbfiles[typ] = file_offsets
#        self._descriptors = []
#        for typ in self.dbfiles:
#            if typ == 'exc':
#                tmp = {k: open(self.dbroot + self.pos[k] + '.exc', 'rb')
#                        for k in self.pos}
#            elif typ == 'data':
#                tmp = {k: open2fd(self.dbroot + typ + '.' + self.pos[k], O_RDONLY)
#                        for k in self.pos}
#                self._descriptors.extend(tmp.values())
#                tmp = {k: mmap(v, 0, prot=PROT_READ) for k, v in tmp.items()}
#            else:
#                tmp = {k: open(self.dbroot + typ + '.' + self.pos[k], 'rb')
#                        for k in self.pos}
#            self.dbfiles[typ] = tmp

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        """
        Safely close database files when we're done.
        See also: Python Context Manager Protocol
        """
        print('exiting',self)

        self.memory.close()
        self.synset_index.close()
#        for typ in self.dbfiles.values():
#            for file_obj in typ.values():
#                file_obj.close()
#        closerange(min(self._descriptors), max(self._descriptors))
        return False


class WordNetManager(WordNetBase, purpose='manager'):
    """
    Class representing the WordNet database, and encapsulates all the
    available operations involving the data.

    Instance Attributes:
        dbroot: The root directory in which the WordNet database files
                reside.  Can be absolute, or relative to the working
                directory of the Python script.
        pos: A dictionary mapping WordNet part of speech abbreviations
             to the longer forms used in the database filenames.
        dbfiles: A dictionary mapping the file types and the parts of
                 speech to the offsets in the mmap containing all the
                 database contents.  Only valid in a Context Manager
                 scope.
    """
    def __init__(self, capabilities_inst, worker_cls, db_root=wn_db_location):
        super().__init__(db_root)
        self.capabilities = capabilities_inst
        self.worker_cls = worker_cls

    def __enter__(self):
        super().__enter__()
        self.capabilities.__enter__()
        self.__dict__.update({name: self.enqueuer(name) for name in dir(self.capabilities)
                if (not name.startswith('_')) and callable(getattr(self.capabilities, name))})

        self._managerPipes = {}
        for i in range(cpu_count()):
            mgr_pipe, sub_pipe = Pipe(duplex=True)
            self._managerPipes[mgr_pipe.fileno()] = mgr_pipe
            psub = self.worker_cls(sub_pipe, self.capabilities, daemon=True)
            psub.start()
            self.available.append(mgr_pipe)
            # Low-level file descriptors are copied when creating a
            # subprocess, so close the copy in the main process now
            # When the child process is signalled, it will close it's
            # end and the manager pipe will signal EOF as expected
            sub_pipe.close()

        self._old_sig_handler = signal(SIGTERM, self.thread_crash_handler)
        self._closed = False
        self.responder = Thread(target=self.manager_exception_handler, daemon=True)
        self.responder.start()

        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        print('closing manager')
        self.requests.clear()
        self.available.clear()

#            kill all subprocesses
        if not self._closed:
            for v in list(self._managerPipes.values()):
                v.send(None)
            self._closed = True
        self.cleanup_workers()

        signal(SIGTERM, self._old_sig_handler)
        self.capabilities.__exit__(exc_type, exc_val, exc_tb)
        super().__exit__(exc_type, exc_val, exc_tb)
        return False

    def enqueuer(self, func_name):
        if func_name is None:
            def wrapper():
                """Poison pill for subprocesses"""
                try:
                    pipe = self.available.popleft()
                    pipe.send(None)
                except IndexError:
                    self.requests.append(None)
        else:
            def wrapper(*args, **kargs):
                self.requestsMade += 1
                req = (func_name, args, kargs)

       #         print('made request for',req)
                wnres = WordNetResult(req)
                self.pending[id(req)] = wnres
                try:
                    pipe = self.available.popleft()
                    pipe.send((id(req), req))
                except IndexError:
                    self.requests.append((id(req), req))
                #Result object, kinda like multiprocessing.Pool
                return wnres
        return wrapper

    def thread_crash_handler(self, sig_no, stack_frame):
        print('handling thread...')
        if hasattr(self, '_crash_info'):
            print('have info')
            def info_interpreter(task_id):
                result_obj = self.pending.get(task_id)
                if result_obj is None:
                    return 'Error retrieving next task.'
                else:
                    return 'Error while computing: {0}'.format(result_obj.req)
            handle_exception_packet(self._crash_info, info_interpreter)
        elif callable(self._old_sig_handler):
            self._old_sig_handler(sig_no, stack_frame)
    def signal_exception(self, _, exc_info):
        """
        Parameters:
            _: Unused parameter, required by the interface of the thread
               which processes requests.
               See Also:  WordNet.requestor, WordNet.manage_requests()
        TODO: Finish documenting this
        """
        print('thread crash',_,exc_info)
        #Why does this turn into an array?
        self._crash_info = exc_info[0]
        kill(current_process().pid, SIGTERM)

    def manager_exception_handler(self):
        try:
            self.manage_requests()
        except Exception as exc:
            self.signal_exception(None,
                    build_exception_packet(exc, id(self._sentinel)))
    def manage_requests(self):

        def process_result(conn, res):
            task_id, res = res
            print('done',self.pending[task_id].req,res,end='</br>')
            self.requestsComplete += 1
            print('completed request\u0010{}\u0017'.format(
                self.requestsComplete-self.requestOffset))
            stdout.flush()
            self.pending[task_id]._data = res
            self.pending.pop(task_id)._evt.set()
            try:
                task = self.requests.popleft()
                conn.send(task)
            except IndexError:
                self.available.append(conn)
        def synset_request(conn, res):
            pass
        message_handlers = {self.message_types['COMPLETED_RESULT']: process_result,
                self.message_types['EXCEPTION']: self.signal_exception,
                self.message_types['SYNSET_REQUEST']: synset_request}

        print('starting thread')
        while self._managerPipes:
            print(self._closed, self.requests)
            print([(k,v.closed) for k,v in self._managerPipes.items()])
            for conn in wait(self._managerPipes.values()):
                print(conn.fileno(),'pinged')
                try:
                    message_type, *res = conn.recv()
                    print('got', message_type, 'from worker', flush=True)
                    message_handlers[message_type](conn, res)
                #It seems that the last-created pipe always exits with
                #a ConnectionResetError rather than EOF.
                #TODO: Figure out why, seems like brittle design
                except (EOFError, ConnectionResetError):
                    print('removed connection',conn.fileno())
                    self._managerPipes.pop(conn.fileno())
                    conn.close()
            print([(k,v.closed) for k,v in self._managerPipes.items()])
        print('thread all done')
        self.cleanup_workers()

    def cleanup_workers(self):
        active_children() #Only used to join finished processes

    def close(self):
        """Stop this WordNet instance from accepting any more tasks"""
        poison_pill = self.enqueuer(None)
        for k in list(self._managerPipes):
            poison_pill()
        self._closed = True

    def join(self, timeout=None):
        """Block until all tasks have completed"""
        print('joining wn')
        if not self._closed:
            raise RuntimeError('Must call close() before join()')
        self.responder.join(timeout)

class WordNetWorker(Process, metaclass=WordNetMeta, purpose='worker'):
    def __init__(self, pipe, capabilities, *args, **kargs):
        super().__init__(*args, **kargs)
        self.pipe = pipe
        self.capabilities = capabilities

    def run(self):
        # TODO: Errors are inconsistently propagated to the owning process.
        #       Need to figure out why!
        try:
            task = self.pipe.recv()
            while task is not None:
                #If an error occurs with a known task_id, then attach
                #the error to the respective result object
                #otherwise, attach it to a sentinel result object which
                #handles 'exceptional' cases
                task_id, (func_name, args, kargs) = task
                print('Received request',func_name,'from',
                        current_process().name, 'with args=', args)
                try:
                    self.pipe.send((self.capabilities.message_types['COMPLETED_RESULT'],
                        task_id, getattr(self.capabilities, func_name)(*args, **kargs)))
                except Exception as exc:
                    self.pipe.send((self.capabilities.message_types['EXCEPTION'],
                        build_exception_packet(exc, task_id)))
                task = self.pipe.recv()
        except Exception as exc:
            self.pipe.send((self.capabilities.message_types['EXCEPTION'],
                build_exception_packet(exc, id(self.capabilities._sentinel))))
        finally:
            #Causes broken pipe error on WordNet.__exit__()
            #if reached due to an exception
            print('closed subprocess',current_process().name)
            self.pipe.close()
        
    def requestor(self, func_name):
        pass


class WordNetCapabilities(WordNetBase, purpose='capability_provider'):
    """
    Class representing the operations WordNet can do.

    WordNet manages multiple such workers, and when an operation is
    requested by a user/programmer, WordNet forwards this to an
    available worker which processes the task and returns a result.

    All file accesses are completed by making a request to the
    owning WordNet Instance which reads the file and sends the data
    to the Worker.

    Alternatively, pass the mmap objects to these Workers so that
    they all access the same objects as they need.  This may prove
    ineffective due to seek time on disk because of 'conflicting'
    read requests from multiple process.  Might be worth at least
    trying if it's not too difficult.
    """
#    def __init__(self, db_root=wn_db_location):
#        super().__init__(db_root)
#    def __init__(self, memory, db_offsets):
#        self.memory = memory
#        self.dbfiles = db_offsets

    def path_between_words(self, word, pos_word, other, pos_other, mapped={}):
        """
        Uses a version of Uniform-Cost Search to traverse the WordNet
        database for a path between two given words.

        Parameters:
            self: The WordNet instance to use
            word: The word to begin the search from
            pos_word: The part of speech to which `word` belongs
            other: The word to use as the destination
            pos_other: The part of speech to which `other` belongs

        Returns:
            The cost of the lowest-cost-path (according to
            default_cost_func() in the utils subpackage) traversed
            between the synset containing the source word `word` and
            the synset containing the target word `other`
            If either `word` or `other` does not occur in the WordNet
            database, or there is no path between the two, the
            algorithm will return None.
        """

        #Preprocess parameter values into expected forms
        if isinstance(word, str):
            word = word.encode('ascii')
        if isinstance(other, str):
            other = other.encode('ascii')
        if isinstance(pos_other, str):
            pos_other_bytes = pos_other.encode('ascii')
        elif isinstance(pos_other, bytes):
            pos_other_bytes = pos_other
            pos_other = pos_other.decode()

        visited = set() # All the synsets already processed

        # Priority queue starting with the synsets word is a part of
        unexplored = [(1, s) for s in self.get_synsets_from_word(word, pos_word)]
        print('syns',word,other, unexplored)

        if not self.word_exists(other, pos_other):
            print('not exists',word,other)
            return None, None

#        print('comparing %s(%s) %s(%s)' % (word, pos_word, other, pos_other))
    
        while unexplored:
            cost, next_synset = heapq.heappop(unexplored)
            if other in next_synset.words and\
                    pos_other_bytes == next_synset.type:
                return cost, next_synset # Found a path
            for ptr in next_synset.pointers:
                if ptr['relation'] == b'0000' and\
                        (ptr['pos'] + ptr['offset']) not in visited:
                    visited.add(ptr['pos'] + ptr['offset'])
                    heapq.heappush(unexplored,
                            (default_cost_func(cost, next_synset),
                        mapped.setdefault(ptr['pos'] + ptr['offset'],
                            self.get_synset_from_offset(
                                int(ptr['offset']), ptr['pos'].decode())).follows(next_synset)))
        else:
            # Did not break in loop, and unexplored queue is empty
            print('no path',word,other)
            return None, None # No relation

    def word_exists(self, word, pos):
        """
        Checks the WordNet database for the presence of the given word.

        Parameters:
            word: The word to search for
            pos: The part of speech to which the word belongs
        Returns:
            True if the word could be found in the database, False
            otherwise
        """
        line, _ = binary_search_index(self.memory, word,
                *self.dbfiles['index'][pos])
        return line is not None

    def get_synsets_from_word(self, word, pos):
        """
        Given a word, retrieve the synsets containing it
        
        Parameters:
            word: The word to search the database for
            pos: The part of speech to which the word belongs
        Returns:
            The synsets to which the given word belongs, or an empty list
            if the word could not be found in the database
        """
        line, _ = binary_search_index(self.memory, word,
                *self.dbfiles['index'][pos])
    
        if line is not None:
            info = line.split()
            # info[2] is the number of senses of the word in WordNet
            for offset in info[-int(info[2]):]:
                offset = int(offset)
# TODO: Parse SynSet once, then pass between Workers.
# TODO: See if it's more time efficient than currently (definitely good for memory tho).
# TODO: Decouple from Capabilities and put in Worker, somehow
#                self.synset_index.seek(offset)
#                if self.synset_index.read_byte() == 0:
#                    self.safe_write.clear()
#                    if self.safe_write.aquire(block=False):
#                        self.synset_index.seek(offset)
#                        self.synset_index.write_byte(self.pipe.fileno())
#                        self.safe_write.set()
#                        self.safe_write.release()
#                    else:
#                        self.safe_write.wait()
                yield self.get_synset_from_offset(int(offset), pos)
        else:
            # Word does not occur in WordNet database
            return ()

    def get_synset_from_offset(self, offset, pos):
        """
        Parses a WordNet database file from the given offset and returns
        a SynSet object containing the parsed information.
        
        Parameters:
            offset: The byte offset to jump to
            pos: The part of speech corresponding to this offset
        Returns:
            A SynSet object containing data parsed from the specified offset
        """
        self.memory.seek(self.dbfiles['data'][pos][0] + offset)
        return SynSet(self.memory.readline().split())
    
    def get_base_word(self, word, pos):
        """
        Given a word, mophologically reduce it to the base form.
        
        For the time being, some non-words are reduced to an existing
        word which is subsequently returned.
        TODO: Consider how to fix this issue.
        
        Parameters:
            word: The word to reduce
            pos: The part of speech to which the word belongs
        Returns:
            A string giving the base form of the word, or None if the word
            cannot be found in the WordNet database.
        """
        if isinstance(word, str):
            word = word.encode('ascii')

        line, _ = binary_search_index(self.memory, word,
                *self.dbfiles['exc'][pos])
        if line is not None:
            bases = line.split()[1:]
            #TODO: Sometimes, words in the exceptions list
            #      are not actually in the WordNet database.
            #      It's not inherently a problem, but it
            #      creates unnecessary search requests later
        else:
            bases = []

        #Consider changing the algorithm to return only this if it exists
        #and otherwise performing the morphological reduction
        if self.word_exists(word, pos):
            bases.append(word)
            
        # Process non-exceptions
        morphemes = {
                'n': {b's':[b''], b'es':[b''], b'ies':[b'y'], b'men':[b'man']},
                'v': {b's':[b''], b'es':[b''], b'ies':[b'y'], b'ing':[b'', b'e'],
                    b'ed':[b'', b'e']},
                'r': {},
                'a': {b'er':[b'', b'e'], b'est':[b'', b'e']}}
        return {base for suffix, substitutes in morphemes[pos].items()
                for base in (word[:-len(suffix)]+sub for sub in substitutes)
                if word.endswith(suffix) and self.word_exists(base, pos)
                }.union(bases)

    def get_part_of_speech(word, *args):
        """
        Find the part of speech to which a word belongs in the
        given context.
            Viterbi Algorithm
            https://en.wikipedia.org/wiki/Dynamic_programming

        Parameters:
            word: The word to analyze
            *args: Placeholder for anything else the algorithm may need
        Returns:
            The WordNet abbreviation representing the part of
            speech to which `word` belongs
            TODO: Actually implement something
        """
        pass

