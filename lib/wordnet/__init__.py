"""
Defines the interface to the WordNet database.
Change in same spot in a different repository.
"""

__version__ = '1.0.0'
__author__ = 'Radomir Wasowski'
__all__ = ['SynSet', 'WordNet']

from os.path import dirname as _OS_dirname, abspath as _OS_abspath, join as _OS_join
wn_db_location = _OS_join(_OS_dirname(_OS_abspath(__file__)), 'dict', 'en', '')

from .interface import SynSet, WordNet
