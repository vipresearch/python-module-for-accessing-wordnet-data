"""
Definitions for any cost functions to use in the path searching
algorithm in the WordNet class.
Also defines the cost function to be used as the default for searches.
"""

__all__ = ['default_cost_func', 'distance_cost', 'fan_out_cost', 'quasi_probability_cost']

def distance_cost(old_cost, synset):
    """Cost is calculated as the length of the traversed path"""
    return old_cost + 1

def fan_out_cost(old_cost, synset):
    """Cost is calculated by the fan-out of each synset (number of pointers)"""
    return old_cost + len(synset.pointers)

def quasi_probability_cost(old_cost, synset):
    """Cost is calculated by the product of the fan out
        See Also: fan_out_cost()"""
    return old_cost * len(synset.pointers)

#default_cost_func = distance_cost
#default_cost_func = fan_out_cost
default_cost_func = quasi_probability_cost
