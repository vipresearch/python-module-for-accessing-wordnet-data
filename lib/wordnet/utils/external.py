"""
Defines utilities for library users, and not expected to be needed
for API development.
"""

__all__ = ['PathRecoveryEncoder']

from json import JSONEncoder

class PathRecoveryEncoder(JSONEncoder):
    """
    A class to convert SynSets into JSON.
    See Also: json.JSONEncoder
    """
    def default(self, obj):
        """
        Default preprocessing logic for serialization.

        Decodes bytes into strings, and converts sets into lists.
        Also converts a SynSet object into a list of dictionaries by
        following the path of SynSets stored in the .prev attribute;
        each list entry has the pos and words attributes of the SynSet,
        and the order of the path is maintained by list index.
        """
        from .. import SynSet # Must be here, to prevent circular imports
        if isinstance(obj, SynSet):
            path = [{'pos': obj.type, 'words': [wd for wd in obj.words.values()]}]
            while obj.prev is not None:
                obj = obj.prev
                path.append({'pos': obj.type, 'words': [wd for wd in obj.words.values()]})
            return path
        elif isinstance(obj, bytes):
            return obj.decode()
        elif isinstance(obj, set):
            return list(obj)
        return super().default(obj)
