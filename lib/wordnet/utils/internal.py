"""
Defines utilities primarily used by the WordNet API itself.
"""

__all__ = ['binary_search_index', 'build_exception_packet', 'handle_exception_packet']

# build_exception_packet()
from io import StringIO
from traceback import print_exception
# handle_exception_packet()
from sys import stderr

def binary_search_index(fileObj, word, lo=0, hi=None):
    """
    Given a file-like object open in binary mode, perform a binary search
    for the line beginning with the given word.  The word must be separated
    from the remainder of the line by whitespace.
    
    Parameters:
        fileObj: An open file-like object supporting seek(), read(),
                 readline(), and tell() operations
        word: A string or bytes object to search for at the beginning of
              lines.  strings are ASCII-encoded into bytes.

    Returns:
        The whole line which begins with the parameter word, or None if the
        search failed.
    """
    initial = fileObj.tell() #To reset state before returning

    if isinstance(word, str):
        word = word.encode('ascii')
    if hi is None:
        hi = fileObj.seek(0, 2)
    pivot = lo
    iterations = 0

    while hi > lo:
        pivot = ((hi-lo)//2) + lo
        fileObj.seek(pivot, 0)
        try:
            # Read back to the beginning of the line
            while fileObj.read(1) != b'\n':
                fileObj.seek(-2, 1)
        except OSError as e:
            if e.errno != 22:
                raise e
            # In this case, we are at the very beginning of the file
            fileObj.seek(0)
        line = fileObj.readline()
        target = line.split(b' ', 1)[0]

        #Tighten search constraints
        if target < word:
            lo = fileObj.tell()
        elif target > word:
            hi = fileObj.tell() - len(line)
        else:
            break
        iterations += 1 # Debugging logs;
        # TODO: remove without breaking the unit test
        # Is there any way to retain that information...?

    fileObj.seek(initial)
    if target == word:
        return line, iterations
    else:
        return None, iterations

def build_exception_packet(exc, *extra_info):
    """
    Create a tuple of information relevant to an exception.
    Used to maintain context information between processes.
    See Also: handle_exception_packet()

    Parameters:
        exc: The exception instance to parse a traceback from.
        extra_info: Varargs which encapsulates any extra context information
                    to send with the packet.

    Returns:
        A tuple containing the exception itself in the first slot, the
        traceback printout as a string in the second slot, and the extra
        context information in the remaining slots.
    """
    print('building packet:',exc,extra_info)
    traceback_out = StringIO()
    print_exception(type(exc), exc, exc.__traceback__, file=traceback_out)
    packet = (exc, traceback_out.getvalue(), *extra_info)
    traceback_out.close()
    return packet

def handle_exception_packet(pack, info_interpreter=None):
    """
    Processes a tuple which contains:
        An exception in the first slot, which is raised after processing.
        A string in the second slot containing a traceback for the exception,
            which is printed to standard error.  See Also: sys.stderr
        Optionally, any additional information necessary for the exception.
    See Also: build_exception_packet()

    Parameters:
        pack: A tuple containing information about an exception
        info_interpreter: (Optional) A callable which will return a string
                          after parsing any extra information supplied in
                          the packet.  A new Exception will be thrown using
                          this string as the message.  When the parameter is
                          omitted, any extra information is ignored and the
                          exception from the packet is re-raised directly.

    Returns:
        No return value, but an exception is always raised from the packet.
    """
    exc, trc, *extra_info = pack
    print(trc, file=stderr)
    if extra_info and info_interpreter is not None:
        raise Exception(info_interpreter(*extra_info)) from exc
    else:
        raise exc
